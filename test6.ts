interface Person {
    firstName: string;
    lastName: string;
    greeter(): string;
//   last(): number;
}

class someOne implements Person {
    public firstName: string;
    public lastName: string;
    private name:string;
    
    constructor(firstName: string, lastName: string) { 
      this.firstName=firstName;
      this.lastName=lastName;
      this.name = this.firstName + " " + this.lastName;
    }
    
    greeter():string {
        return "Hello, " + this.firstName + " " + this.lastName;
    }   
}

let a = new someOne("magnus", "kronnas");

console.log(a.greeter());
