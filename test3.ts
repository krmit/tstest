interface Person {
    firstName: string;
    lastName: string;
}

function greeter(person:Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

//let user = { firstName: "Jane", lastName: "User" };
let user = { firstNamn: "Jane", lastName: "User" };

console.log(greeter(user));
