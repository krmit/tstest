interface I {
    foo(): string;
    baa(): string;
}

class B {
    protected name:string;
    
    constructor() { 
      this.name = "Test B";
    }
    
    baa():string {
        return "Baa test: " + this.name+Number();
    }   
}

class A extends B implements I {
    
    constructor() {
      super(); 
      this.name = "Test A";
    }
    
    foo():string {
        return "Foo test: " + this.name+Number();
    }   
}

let a = new A();

console.log(a.foo());
console.log(a.baa());
