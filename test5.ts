interface Person {
    firstName: string;
    lastName: string;
}

class someOne implements Person {
    firstName: string;
    lastName: string;
    
    constructor(firstName: string, lastName: string) { 
    this.firstName=firstName;
    this.lastName=lastName;
    }
    
    greeter():string {
        return "Hello, " + this.firstName + " " + this.lastName;
    }   
}

let a = new someOne("magnus", "kronnas");

console.log(a.greeter());
